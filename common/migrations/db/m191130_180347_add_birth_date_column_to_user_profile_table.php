<?php

use yii\db\Migration;

/**
 * Handles adding birth_date to table `user_profile`.
 */
class m191130_180347_add_birth_date_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'birth_date', $this->date()->comment('Дата рождения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'birth_date');
    }
}
