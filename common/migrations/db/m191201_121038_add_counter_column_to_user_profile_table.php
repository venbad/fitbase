<?php

use yii\db\Migration;

/**
 * Handles adding counter to table `user_profile`.
 */
class m191201_121038_add_counter_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'counter', $this->integer()->defaultValue(0)->comment('Счетчик'));
        $this->addColumn('{{%user_profile}}', 'counter_last_click', $this->integer()->comment('Дата и время последнего клика на счетчик'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'counter');
        $this->dropColumn('{{%user_profile}}', 'counter_last_click');
    }
}
