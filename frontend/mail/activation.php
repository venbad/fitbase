<?php
/**
 * @var $this \yii\web\View
 * @var $url \common\models\User
 * @var $pass string
 */
?>
<p>
<?php echo Yii::t('frontend', 'Your activation link: {url}', ['url' => Yii::$app->formatter->asUrl($url)]) ?>
</p>
<p>
<?php echo Yii::t('frontend', 'Your password : {pass}', ['pass' => $pass]) ?>
</p>
