<?php

namespace frontend\controllers;

use common\models\UserProfile;
use frontend\modules\user\models\LoginForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Counter controller
 */
class CounterController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ]
            ]
        ];
    }

    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $first = (int)UserProfile::find()
                    ->select('user_id')
                    ->orderBy(['counter' => SORT_DESC])
                    ->scalar() === (int)$model->getUser()->id;

            $user = UserProfile::findOne($model->getUser()->id);

            $dataProvider = new ArrayDataProvider([
                'allModels' => UserProfile::find()->where(['!=', 'user_id', $model->getUser()->id])->all(),
                'pagination' => false
            ]);

            return $this->render('index', [
                'first' => $first,
                'counter' => $user->counter,
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->render('form', [
            'model' => $model
        ]);
    }

}
