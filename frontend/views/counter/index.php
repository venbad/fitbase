<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */
/* @var $first boolean */
/* @var $counter integer */
/* @var $counter integer */
/* @var $dataProvider \yii\data\ArrayDataProvider */

$this->title = 'Счетчик';
?>
<div class="site-login">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <span style="font-size: 90px" class="<?php echo $first ? 'text-success' : 'text-danger' ?>"><?php echo $counter ?></span>
    <div class="row">
        <?php echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'user.email',
                'counter',
                'counter_last_click:datetime',
                ],
            ])?>
    </div>
</div>
